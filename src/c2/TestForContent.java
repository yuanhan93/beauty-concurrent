package c2;

/**
 * 数组内数组元素的内存地址是连续的，当访问第一个元素时，会把第一个元素后的若干元素一块放入缓存行
 * 这样顺序访问数组元素会在缓存中直接命中.
 * 执行时间10ms以下
 */
public class TestForContent {
    private static final int LINE_NUM=1024;
    private static final int COLUMN_NUM=1024;
    public static void main(String[] args){
        long [][] array=new long[LINE_NUM][COLUMN_NUM];
        long startTime=System.currentTimeMillis();
        for(int i=0;i<LINE_NUM;i++){
            for(int j=0;j<COLUMN_NUM;j++){
                array[i][j]=i*2+j;
            }
        }
        long endTime=System.currentTimeMillis();
        long cacheTime=endTime-startTime;
        System.out.println("cache time:"+cacheTime+"ms");
    }
}
