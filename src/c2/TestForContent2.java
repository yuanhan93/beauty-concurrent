package c2;

/**
 * 跳跃式访问数组元素，破坏了程序访问的局部性原则。
 * 缓存是有容量控制的，当缓存满了会根据一定的淘汰算法替换缓存行
 * 执行时间10ms以上
 */
public class TestForContent2 {
    private static final int LINE_NUM=1024;
    private static final int COLUMN_NUM=1024;
    public static void main(String[] args){
        long [][] array=new long[LINE_NUM][COLUMN_NUM];
        long startTime=System.currentTimeMillis();
        for(int i=0;i<COLUMN_NUM;i++){
            for(int j=0;j<LINE_NUM;j++){
                array[j][i]=i*2+j;
            }
        }
        long endTime=System.currentTimeMillis();
        long cacheTime=endTime-startTime;
        System.out.println("cache time:"+cacheTime+"ms");
    }
}
