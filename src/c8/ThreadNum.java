package c8;

/**
 * 线程池线程个数最佳实践
 * 一般情况下
 * 如果是CPU密集型任务，就需要尽量压榨CPU，参考值可以设为 NCPU+1
 * 如果是IO密集型任务，参考值可以设置为2*NCPU
 *
 */
public class ThreadNum {
    public static void main(String[] args){
        int cpu=Runtime.getRuntime().availableProcessors();
        System.out.println("cpu:"+cpu);
        //线程数最佳公式
        //Nthreads=Ncpu*Ucpu*(1+W/C)
        //cpu个数*目标cpu使用率*(1+等待时间与计算时间的比率)
        int threads=(int)(cpu*1*(1+1.5/0.5));
        System.out.println("thread size:"+threads);
    }
}
