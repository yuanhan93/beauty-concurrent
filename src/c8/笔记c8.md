#### 第八章 java并发包中的线程池ThreadPoolExecutor

1. 线程池
  - 解决两个问题
    - 当执行大量异步任务时线程池能提供较好的性能
    - 提供了一种资源限制和管理的手段
  - newCachedThreadPool 线程池线程个数最多可达Integer.MAX_VALUE,线程自动回收
  - newFixedThreadPool 固定大小的线程池
  - newSingleThreadExecutor 单个线程
  - 成员变量原子Integer ctl表示线程池状态和线程池个数。
     - 假设Integer类型用32位表示，高3位用来表示线程池状态，后面29位用来记录线程池线程个数。
  - 线程池参数
    - corePoolSize:线程池核心线程参数
    - workQueue:用来保存等待执行的任务的阻塞队列。
      - 基于数组的有界ArrayBlockingQueue
      - 基于链表的无界LinkedBlockingQueue
      - 最多只有一个元素的同步队列SynchronousQueue
      - 优先级队列PriorityBlockingQueue
    - maximumPoolSize:线程池最大线程数量
    - ThreadFactory:创建线程的工厂
    - RejectedExecutionHandle:饱和策略，当队列满并且线程个数达到maximumPoolSize采取的策略
      - 内置的四种拒绝策略：
      - AbortPolicy:直接抛出异常
      - CallerRunsPolicy:使用调用者所在线程来运行任务
      - DiscardOldestPolicy:调用Poll丢弃一个任务，执行当前任务
      - DiscardPolicy:默默丢弃，不抛出异常
    - keepAliveTime: 存活时间
    - TimeUnit:存活时间的时间单位  
  - 每个worker线程可以处理多个任务
  - execute和submit都可以开启线程池中的任务，但是submit返回的是Future对象，可以通过future的get方法获取异常信息
  - 线程池核心线程个数设置参考：
    一般情况下
     * 如果是CPU密集型任务，就需要尽量压榨CPU，参考值可以设为 NCPU+1
     * 如果是IO密集型任务，参考值可以设置为2*NCPU