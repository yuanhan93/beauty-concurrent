package c8;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 来自《java高并发程序设计》
 * 简单测试下几种ThreadPool
 */
public class ThreadPoolTest1 {
    public static class MyTask implements Runnable{

        @Override
        public void run() {
            System.out.println(System.currentTimeMillis()+":Thread ID:"+Thread.currentThread().getId());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args){
        MyTask task=new MyTask();
        //5个
        ExecutorService es=Executors.newFixedThreadPool(5);
        //有几个就创建几个
        //ExecutorService es=Executors.newCachedThreadPool();
        //单个线程
        //ExecutorService es=Executors.newSingleThreadExecutor();
        for(int i=0;i<10;i++){
            es.submit(task);
        }
    }
}
