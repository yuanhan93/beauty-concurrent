package c8;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 一个求商的例子测试异常堆栈信息
 */
public class DivTask implements Runnable{
    int a,b;
    public DivTask(int a,int b){
        this.a=a;
        this.b=b;
    }
    @Override
    public void run() {
        double re=a/b;
        System.out.println(re);
    }
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //ThreadPoolExecutor pool=new ThreadPoolExecutor(0,Integer.MAX_VALUE,0L,TimeUnit.MILLISECONDS,new SynchronousQueue<>());
        //第三种，自定义线程池打印堆栈信息
        ThreadPoolExecutor pool=new TraceThreadPoolExecutor(0,Integer.MAX_VALUE,0L,TimeUnit.MILLISECONDS,new SynchronousQueue<>());
        for(int i=0;i<5;i++){
            //submit异常信息不显示
            //pool.submit(new DivTask(100,i));
            //第一种，改成execute
            pool.execute(new DivTask(100,i));
            //第二种，改造submit
            //Future ft=pool.submit(new DivTask(100,i));
            //ft.get();
        }
    }
}
