package c11;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * SimpleDateFormat 线程不安全，会发生异常
 */
public class TestSimpleDateFormat {
    //创建单例实例
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println(sdf.parse("2018-12-18 16:06:12"));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }
    }
}
