package c11;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * putIfAbsent
 * key已经存在则直接返回原来的value，不存在则放入value并且返回null
 */
public class TestMap2 {
    //创建map,key为topic,value为设备列表
    public static ConcurrentHashMap<String,List<String>> map=new ConcurrentHashMap<>();

    public static void main(String[] args) {
        //进入直播间topic1,线程one
        Thread threadOne=new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> list=new ArrayList<>();
                list.add("device1");
                list.add("device2");
                List<String> oldList=map.putIfAbsent("topic1",list);
                if(null!=oldList){
                    oldList.addAll(list);
                }
                System.out.println(map);
            }
        });

        //进入直播间topic1,线程two
        Thread threadTwo=new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> list=new ArrayList<>();
                list.add("device11");
                list.add("device22");
                List<String> oldList=map.putIfAbsent("topic1",list);
                if(null!=oldList){
                    oldList.addAll(list);
                }
                System.out.println(map);
            }
        });

        //进入直播间topic2,线程three
        Thread threadThree=new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> list=new ArrayList<>();
                list.add("device111");
                list.add("device222");
                List<String> oldList=map.putIfAbsent("topic2",list);
                if(null!=oldList){
                    oldList.addAll(list);
                }
                System.out.println(map);
            }
        });
        threadOne.start();
        threadTwo.start();
        threadThree.start();
    }
}
