package c11;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 没有调用remove方法会导致内存泄露
 * 1、内存泄漏memory leak :是指程序在申请内存后，无法释放已申请的内存空间，一次内存泄漏似乎不会有大的影响，但内存泄漏堆积后的后果就是内存溢出。
 * 2、内存溢出 out of memory :指程序申请内存时，没有足够的内存供申请者使用，或者说，给了你一块存储int类型数据的存储空间，但是你却存储long类型的数据，那么结果就是内存不够用，此时就会报错OOM,即所谓的内存溢出。
 * 可以用jdk自带的jconsole查看堆的使用情况，在安装目录的bin目录下jconsole.exe
 */
public class ThreadPoolTest {
    private static class LocalVariable{
        private Long[] a=new Long[1024*1024];
    }
    private static ThreadPoolExecutor poolExecutor=new ThreadPoolExecutor(5,5,1,TimeUnit.MINUTES,
            new LinkedBlockingQueue<>());
    private static ThreadLocal<LocalVariable> threadLocal=new ThreadLocal<>();

    public static void main(String[] args) throws InterruptedException {
        for(int i=0;i<50;i++){
            poolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    threadLocal.set(new LocalVariable());
                    System.out.println("use local variable");
                    //threadLocal.remove();
                }
            });
            Thread.sleep(1000);
        }
        System.out.println("pool execute over...");
    }
}
