package c11;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * put会覆盖掉原来的值
 */
public class TestMap {
    //创建map,key为topic,value为设备列表
    public static ConcurrentHashMap<String,List<String>> map=new ConcurrentHashMap<>();

    public static void main(String[] args) {
        //进入直播间topic1,线程one
        Thread threadOne=new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> list=new ArrayList<>();
                list.add("device1");
                list.add("device2");
                map.put("topic1",list);
                System.out.println(map);
            }
        });

        //进入直播间topic1,线程two
        Thread threadTwo=new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> list=new ArrayList<>();
                list.add("device11");
                list.add("device22");
                map.put("topic1",list);
                System.out.println(map);
            }
        });

        //进入直播间topic2,线程three
        Thread threadThree=new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> list=new ArrayList<>();
                list.add("device111");
                list.add("device222");
                map.put("topic2",list);
                System.out.println(map);
            }
        });
        threadOne.start();
        threadTwo.start();
        threadThree.start();
    }
}
