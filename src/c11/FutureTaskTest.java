package c11;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 由于队列元素只有一个，在第一个线程执行的5秒内，第二个线程被阻塞到队列，第三个线程因为是DiscardPolicy拒绝策略被无情抛弃
 */
public class FutureTaskTest {
    //线程池单个线程，队列元素为1,拒绝策略为DiscardPolicy
    private static ThreadPoolExecutor executor=new ThreadPoolExecutor(1,1,1L,TimeUnit.MINUTES,
            new ArrayBlockingQueue<Runnable>(1),new ThreadPoolExecutor.DiscardPolicy());

    public static void main(String[] args) throws Exception{
        Future futureOne=executor.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("start runnable one");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Future futureTwo=executor.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("start runnable two");
            }
        });

        Future futureThree=null;
        try {
            futureThree=executor.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("start runnable three");
                }
            });
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        System.out.println("task One: "+futureOne.get());
        System.out.println("task Two: "+futureTwo.get());
        System.out.println("task Three: "+ (futureThree != null ? futureThree.get() : null));

        executor.shutdown();
    }
}
