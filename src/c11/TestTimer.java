package c11;

import java.util.Timer;
import java.util.TimerTask;

public class TestTimer {
    private static Timer timer = new Timer();

    public static void main(String[] args) {
        //添加任务1，延迟500ms执行
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("---one task---");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //模拟发生异常
                throw new RuntimeException("error...");
            }
        }, 500);

        //添加任务2，延迟1000ms执行
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                for (; ; ) {
                    System.out.println("---two task---");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 1000);

    }
}
