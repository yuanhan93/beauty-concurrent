package c11;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 用悲观锁的方式解决线程安全问题
 */
public class TestSimpleDateFormat2 {
    //创建单例实例
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        synchronized (sdf){
                            System.out.println(sdf.parse("2018-12-18 16:06:12"));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }
    }
}
