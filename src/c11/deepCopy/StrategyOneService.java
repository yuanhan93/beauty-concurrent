package c11.deepCopy;

import java.util.List;

public class StrategyOneService implements StrategyService{
    @Override
    public void sendMsg(List<Msg> msgList, List<String> deviceIdList) {
        for(Msg msg:msgList){
            msg.setDataId("oneService_"+msg.getDataId());
            System.out.println(msg.getDataId()+":"+deviceIdList);
        }
    }
}
