package c11.deepCopy;

import java.util.List;

public interface StrategyService {
    void sendMsg(List<Msg> msgList, List<String> deviceIdList);
}
