package c11.deepCopy;

import java.util.List;

public class StrategyTwoService implements StrategyService{
    @Override
    public void sendMsg(List<Msg> msgList, List<String> deviceIdList) {
        for(Msg msg:msgList){
            msg.setDataId("twoService_"+msg.getDataId());
            System.out.println(msg.getDataId()+":"+deviceIdList);
        }
    }
}
