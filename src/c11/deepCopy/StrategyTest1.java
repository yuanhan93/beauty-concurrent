package c11.deepCopy;

import java.util.*;

/**
 * 测试消息发送，模拟下游数据被修改
 * 根据不同的appkey选择不同的发送策略
 * 共用的msg的id在service中被直接替换，导致出现不正确数据的情况
 */
public class StrategyTest1 {
   private static Map<Integer,StrategyService> serviceMap=new HashMap<>();
   static {
       serviceMap.put(111,new StrategyOneService());
       serviceMap.put(222,new StrategyTwoService());
   }
   public static void main(String[] args){
       Map<Integer,List<String>> appKeyMap=new HashMap<>();
       //创建appKey=1的消息列表
       List<String> oneList=new ArrayList<>();
       oneList.add("device_id1");
       appKeyMap.put(111,oneList);

       //创建appKey=2的消息列表
       List<String> twoList=new ArrayList<>();
       twoList.add("device_id2");
       appKeyMap.put(222,twoList);

       //创建消息
       List<Msg> msgList=new ArrayList<>();
       Msg msg=new Msg();
       msg.setDataId("abc");
       msg.setBody("hello,二狗子");
       msgList.add(msg);

       //根据不同的key使用不同的策略进行处理
       Iterator<Integer> appKeyItr= appKeyMap.keySet().iterator();
       while(appKeyItr.hasNext()){
           int appKey=appKeyItr.next();
           StrategyService strategyService=serviceMap.get(appKey);
           if(null !=strategyService){
               strategyService.sendMsg(msgList,appKeyMap.get(appKey));
           }else{
               System.out.println(String.format("appKey:%s,is not registered service",appKey));
           }
       }

   }
}
