package c11;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TestScheduledThreadPoolExecutor {
    private static ScheduledThreadPoolExecutor scheduledThreadPoolExecutor=new ScheduledThreadPoolExecutor(1);

    public static void main(String[] args) {
        //添加任务1，延迟500ms执行
        scheduledThreadPoolExecutor.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("---one task---");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //模拟发生异常
                throw new RuntimeException("error...");
            }
        }, 500,TimeUnit.MILLISECONDS);

        //添加任务2，延迟1000ms执行
        scheduledThreadPoolExecutor.schedule(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i<5;i++ ) {
                    System.out.println("---two task---");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 1000,TimeUnit.MILLISECONDS);

        scheduledThreadPoolExecutor.shutdown();

    }

}
