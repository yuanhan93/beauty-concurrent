package c11;

/**
 * 不指定线程名称根本看不出来哪个模块的线程出的问题
 * 线程默认名称来源于Thread方法，可以在构造方法里修改name
 *
 */
public class TestThread {
    private static final String THREAD_SAVE_ORDER="THREAD_SAVE_ORDER";
    private static final String THREAD_SAVE_ADDER="THREAD_SAVE_ADDER";
    public static void main(String[] args){
        //订单模块
        //Thread(Runnable target, String name)
        Thread threadOrder=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("保存订单的线程");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                throw new NullPointerException();
            }
        },THREAD_SAVE_ORDER);
        //发货模块
        Thread threadDeliver=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("保存收货地址的线程");
            }
        },THREAD_SAVE_ADDER);

        threadOrder.start();
        threadDeliver.start();
    }
}
