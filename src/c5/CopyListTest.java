package c5;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * CopyOnWriteArrayList迭代器的弱一致性测试
 */
public class CopyListTest {
    private static volatile CopyOnWriteArrayList<String> arrayList=new CopyOnWriteArrayList<>();
    public static void main(String[] args) throws InterruptedException {
        arrayList.add("hello");
        arrayList.add("二狗子");
        arrayList.add("欢迎");
        arrayList.add("来到");
        arrayList.add("java");
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                arrayList.set(1,"三狗子");
                arrayList.remove(2);
                arrayList.remove(3);
            }
        });

        //保证修改线程启动前获取迭代器
        Iterator<String> iterator=arrayList.iterator();
        thread.start();
        thread.join();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
