package c4;

import java.util.concurrent.atomic.AtomicLong;

public class AtomicTest {
    // 创建Long型原子计数器
    private static AtomicLong atomicLong=new AtomicLong();
    //创建数据源
    private static Integer[] arrayOne=new Integer[]{0,1,2,3,0,5,6,0,56,0};
    private static Integer[] arrayTwo=new Integer[]{0,1,2,3,0,5,6,0,56,0};

    public static void main(String[] args) throws InterruptedException {
        //threadOne中统计arrayOne中0的个数
        Thread threadOne=new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<arrayOne.length;i++){
                    if(arrayOne[i]==0){
                        //原始值+1
                        atomicLong.incrementAndGet();
                    }
                }
            }
        });

        //threadTwo中统计arrayTwo中0的个数
        Thread threadTwo=new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<arrayTwo.length;i++){
                    if(arrayTwo[i]==0){
                        //原始值+1
                        atomicLong.incrementAndGet();
                    }
                }
            }
        });

        threadOne.start();
        threadTwo.start();
        threadOne.join();
        threadTwo.join();
        System.out.println("count 0:"+atomicLong.get());
    }

}
