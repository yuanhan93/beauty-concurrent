package c7;

import java.util.Random;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class TestDelay {
    static class DelayedEle implements Delayed{
        private final long delayTime; //延迟时间
        private final long expire;    //到期时间
        private String taskName;      //任务名称

        public DelayedEle(long delay,String taskName){
            this.delayTime=delay;
            this.taskName=taskName;
            this.expire=System.currentTimeMillis()+delay;
        }

        /**
         * 剩余时间,到期时间-当前时间
         * @param unit
         * @return
         */
        @Override
        public long getDelay(TimeUnit unit) {
           return unit.convert(this.expire-System.currentTimeMillis(),TimeUnit.MILLISECONDS);
        }

        @Override
        public int compareTo(Delayed o) {
           return (int)(this.getDelay(TimeUnit.MILLISECONDS)-o.getDelay(TimeUnit.MILLISECONDS));
        }

        @Override
        public String toString() {
            final StringBuilder sb=new StringBuilder("DelayedEle{");
            sb.append("delay=").append(delayTime);
            sb.append(",expire=").append(expire);
            sb.append(",taskName='").append(taskName).append('\'');
            sb.append("}");
            return sb.toString();
        }
    }

    public static void main(String[] args){
        DelayQueue<DelayedEle> delayQueue=new DelayQueue<>();
        Random random=new Random();
        for(int i=0;i<10;i++){
            DelayedEle element=new DelayedEle(random.nextInt(500),"task:"+i);
            delayQueue.offer(element);
        }
        DelayedEle ele=null;
        //依次取出任务并打印
        try{
            for(;;){
                while((ele=delayQueue.take())!=null){
                    System.out.println(ele.toString());
                }
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }

    }
}
