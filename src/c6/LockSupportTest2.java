package c6;

import java.util.concurrent.locks.LockSupport;

public class LockSupportTest2 {
    public static void main(String[] args){
        System.out.println("begin park");
        //让线程先获取许可证
        LockSupport.unpark(Thread.currentThread());
        LockSupport.park();
        System.out.println("end park");
    }
}
