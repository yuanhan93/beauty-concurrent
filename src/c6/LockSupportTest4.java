package c6;

import java.util.concurrent.locks.LockSupport;

public class LockSupportTest4 {
    public static void main(String[] args) throws InterruptedException {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("child thread begin park");
                //调用park方法，挂起自己，只有中断才会退出循环
                while(!Thread.currentThread().isInterrupted()){
                    LockSupport.park();
                }
                System.out.println("child thread unpark");
            }
        });
        thread.start();
        Thread.sleep(1000);
        System.out.println("main thread begin unpark");
        //此时unpark已经无效，只有中断才能结束
        //LockSupport.unpark(thread);
        thread.interrupt();
    }
}
