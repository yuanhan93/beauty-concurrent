package c6;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * 自定义实现不可重入的独占锁
 * 实现并发包下的Lock接口
 * state=0表示锁没有被线程持有
 * state=1表示锁被某一个线程持有
 */
public class NoReentrantLock implements Lock,java.io.Serializable {
    //定义一个内部帮助类
    private static class Sync extends AbstractQueuedSynchronizer{
        //是否锁已经持有
        @Override
        protected boolean isHeldExclusively() {
            return getState()==1;
        }

        /**
         * 如果state=0,尝试获取锁
         */
        @Override
        protected boolean tryAcquire(int acquires) {
            assert acquires == 1;
            //如果state=0则尝试获取锁
            if(compareAndSetState(0,1)){
                //设置独占锁
               setExclusiveOwnerThread(Thread.currentThread());
               return true;
            }
            return false;
        }

        /**
         * 尝试释放锁，设置state=0
         */
        @Override
        protected boolean tryRelease(int releases) {
            assert releases == 1;
            if(getState()==0){
                throw new IllegalMonitorStateException();
            }
            setExclusiveOwnerThread(null);
            setState(0);
            return true;
        }

        /**
         * 提供条件变量接口
         */
        Condition newCondition(){
            return new ConditionObject();
        }
    }
    //创建一个Sync对象来做具体的工作
    private final Sync sync=new Sync();
    @Override
    public void lock() {
        sync.acquire(1);
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        sync.acquireInterruptibly(1);
    }

    @Override
    public boolean tryLock() {
        return sync.tryAcquire(1);
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return sync.tryAcquireNanos(1,unit.toNanos(time));
    }

    @Override
    public void unlock() {
        sync.release(1);
    }

    @Override
    public Condition newCondition() {
        return sync.newCondition();
    }
}
