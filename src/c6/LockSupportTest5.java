package c6;

import java.util.concurrent.locks.LockSupport;

/**
 * 使用带blocker参数的park方法，线程堆栈可以提供更多有关阻塞对象的信息
 */
public class LockSupportTest5 {
    public void testPark(){
        LockSupport.park(this);
    }

    public static void main(String[] args){
        LockSupportTest5 test5=new LockSupportTest5();
        test5.testPark();
    }
}
