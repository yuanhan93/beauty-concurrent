package c6;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 利用ReentrantLock实现简单的线程安全的list
 */
public class ReentrantLockListTest {
    private ArrayList<String> array = new ArrayList<>();
    //默认是非公平锁
    private volatile ReentrantLock lock = new ReentrantLock();

    private void add(String s) {
        lock.lock();
        try {
            array.add(s);
        } finally {
            lock.unlock();
        }
    }

    private void remove(String s) {
        lock.lock();
        try {
            array.remove(s);
        } finally {
            lock.unlock();
        }
    }

    private void get(int index) {
        lock.lock();
        try {
            array.get(index);
        } finally {
            lock.unlock();
        }
    }
}
