package c6;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 先获取锁
 * 当线程调用变量的await方法时，在内部构造一个Node.CONDITION的node节点，并插入到条件队列末尾，之后当前线程会释放获取的锁
 * 当线程调用条件变量的signal方法时，在内部会把条件队列里队列头的一个线程节点从条件队列里移除并且放入AQS的阻塞队列里，然后激活这个线程
 */
public class ConditionTest1 {
    public static void main(String[] args){
        //创建一个独占锁对象ReentrantLock
        ReentrantLock lock=new ReentrantLock();
        //创建一个条件变量，一个lock可以创建多个
        Condition condition=lock.newCondition();
        //获取独占锁
        lock.lock();

        try{
            System.out.println("begin wait");
            condition.await();
            System.out.println("end wait");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }

        lock.lock();

        try{
            System.out.println("begin signal");
            condition.signal();
            System.out.println("end signal");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }

    }
}
