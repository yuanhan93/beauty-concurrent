package c6;

import java.util.concurrent.locks.LockSupport;

public class LockSupportTest1 {
    public static void main(String[] args){
        System.out.println("begin park");
        //为了线程调度，禁用当前线程，除非许可可用
        LockSupport.park();
        System.out.println("end park");
    }
}
