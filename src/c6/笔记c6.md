#### 第六章 java并发包中锁原理

1. LockSupport工具类
   - 主要作用是挂起和唤醒线程
   - LockSupport类与每个使用它的线程都会关联一个许可证
   - 使用Unsafe类实现
   - park()方法
     - 为了线程调度，禁用当前线程，除非许可可用
     - 如果调用park方法的线程已经拿到了LockSupport关联的许可证，则调用LockSupport.park()时马上返回，
    否则调用线程会被禁止参与线程的调度。
     - 阻塞线程被返回情况：unpark方法，interrupt方法，虚假唤醒
   - unpark(Thread thread)方法
     - 当一个线程调用unpark方法时，如果参数thread线程没有持有thread与LockSupport类关联的许可证，
    则让thread线程持有。
     - ① LockSupport不需要在同步代码块里 。所以线程间也不需要维护一个共享的同步对象了，实现了线程间的解耦。
     - ② unpark函数可以先于park调用，所以不需要担心线程间的执行的先后顺序。
   - parkNanos(long nanos)
     - 如果没有拿到许可证，则调用线程会被挂起nanos时间后修改为自动返回
   - park(Object blocker)
     - blocker对象会被记录到该线程内部
     - 常用于线程堆栈排查哪个类被阻塞了
     - 查看线程阻塞情况，先jps查看本机jvm进程，再通过jstack 进程id, 找到类的堆栈信息
   - parkNanos(Object blocker,long nanos)
     - 多了个超时时间
   - parkUntil(Object blocker,long deadline)
     - deadline单位为ms,从1970年到现在某一个时间点的毫秒值
     
2. 抽象同步队列AQS（AbstractQueuedSynchronizer）
   - 实现同步器的基础组件
   - first-in-first-out (FIFO) wait queues ，一个FIFO双向队列
   - 内部通过节点head和tail记录队首和队尾元素
   - 队列元素类型为Node
   - waitStatus记录当前线程的等待状态
      - CANCELLED（1）: 线程被取消 
      - SIGNAL（-1）: 线程需要被唤醒
      - CONDITION(-2): 线程在条件队列里等待
      - PROPAGATE（-3）: 释放共享资源时需要通知其他节点
   - ConditionObject
      - AQS的一个内部类，用来结合锁实现线程同步
      - 是条件变量，每个条件变量对应一个条件队列（单向链表对列），用来存放调用条件变量的await方法后被阻塞的线程
   - 条件变量的支持
      - signal和await方法用来配合AQS实现的锁实现线程间同步
   - 一个锁对应一个AQS阻塞队列，对应多个条件变量，每个条件变量都有自己的一个条件队列
   
3. ReentrantLock
   - 使用AQS实现的可重入独占锁
   - AQS状态值=0表示当前锁空闲，大于等于1说明该锁被占用
   - 锁内部有公平和非公平实现，默认是非公平
   - 公平锁：FairSync重写的tryAcquire方法，加了一个公平性策略方法hasQueuedPredecessors.
   - lock 当没获取锁的情况下会丢到阻塞队列，tryLock,当没获取锁的时候不放入阻塞队列
4. ReentrantReadWriteLock
   - 采用读写分离的策略，允许多个线程同时获取读锁 
   - AQS状态值的高16位表示读锁的个数，低16位表示获取写锁的线程的可重入个数  
5. StampedLock
   - java8新增
   - 三种模式的读写控制，写锁writeLock,悲观读锁readLock,乐观读锁tryOptimisticRead,
   都是不可重入锁
   - 悲观读锁，操作数据前会悲观的认为其他线程可能要对自己操作的数据进行修改。
   - 乐观读锁，操作数据前并没有通过CAS设置锁的状态，仅仅通过位运算测试。
   在多线程多读的情况下提供了更好的性能
   - 支持三种锁互相转换
   - stamp(戳记)代表锁的状态，long型变量
   - 该锁不是直接实现Lock或者ReadWriteLock接口，而是自己内部维护了一个双向阻塞队列
   