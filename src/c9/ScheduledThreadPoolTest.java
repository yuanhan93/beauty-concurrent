package c9;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 来自《java高并发程序设计》
 * 测试两种不同的定时策略
 */
public class ScheduledThreadPoolTest {
    public static void main(String[] args){
        ScheduledExecutorService ses= Executors.newScheduledThreadPool(10);
        //任务执行3秒，任务频率2秒一次，打印时间间隔3秒
        /*ses.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("当前时间（s）:"+System.currentTimeMillis()/1000);
            }
        },0,2,TimeUnit.SECONDS);*/

        //任务执行3秒，延迟2秒，打印时间间隔5秒
        ses.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("当前时间（s）:"+System.currentTimeMillis()/1000);
            }
        },0,2,TimeUnit.SECONDS);
    }
}
