package c1;

/**
 * 守护线程测试
 */
public class DaemonThreadTest {
    public static void main(String[] args){
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                for(;;){

                }
            }
        });

        //设置为守护线程
        thread.setDaemon(true);
        thread.start();
        System.out.println("main is over");
    }
}
