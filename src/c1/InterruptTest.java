package c1;

/**
 * 结果：
 * ...
 * Thread[Thread-0,5,main]: hello
 * Thread[Thread-0,5,main]: hello
 * Thread[Thread-0,5,main]: hello
 * main thread interrupt thread
 * Thread[Thread-0,5,main]: hello
 * main is over
 * 线程的中断标志并不能直接终止该线程的执行
 */
public class InterruptTest {
    public static void main(String[] args) throws InterruptedException {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()){
                    System.out.println(Thread.currentThread()+": hello");
                }
            }
        });

        thread.start();
        Thread.sleep(1000);
        System.out.println("main thread interrupt thread");
        thread.interrupt();
        thread.join();
        System.out.println("main is over");
    }
}
