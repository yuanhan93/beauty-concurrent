package c1;

/**
 * 其中一种结果
 * threadB get resourceA lock
 * threadB begin wait
 * threadA get resourceA lock
 * threadA begin wait
 * threadC begin notify
 * threadB end wait
 */
public class NotifyTest {
    //创建资源
    private static volatile Object resourceA=new Object();

    public static void main(String[] args) throws InterruptedException {
        //创建线程
        Thread threadA=new Thread(new Runnable() {
            @Override
            public void run() {
                //获取resourceA共享资源的监视器锁
                synchronized (resourceA){
                    System.out.println("threadA get resourceA lock");
                    try{
                        System.out.println("threadA begin wait");
                        resourceA.wait();
                        System.out.println("threadA end wait");
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }

                }
            }
        });

        //创建线程
        Thread threadB=new Thread(new Runnable() {
            @Override
            public void run() {
                //获取resourceA共享资源的监视器锁
                synchronized (resourceA){
                    System.out.println("threadB get resourceA lock");
                    try{
                        System.out.println("threadB begin wait");
                        resourceA.wait();
                        System.out.println("threadB end wait");
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }

                }
            }
        });

        //创建线程
        Thread threadC=new Thread(new Runnable() {
            @Override
            public void run() {
               synchronized (resourceA){
                   System.out.println("threadC begin notify");
                   //唤醒其中一个
                   resourceA.notify();
                   //唤醒所有
                   //resourceA.notifyAll();
               }
            }
        });

        threadA.start();
        threadB.start();
        Thread.sleep(1000);
        threadC.start();
        //等待线程结束
        threadA.join();
        threadB.join();
        threadC.join();

        System.out.println("main over");
    }
}
