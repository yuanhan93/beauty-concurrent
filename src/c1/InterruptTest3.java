package c1;

/**
 * 结果：
 * isInterrupted: true
 * isInterrupted: false
 * isInterrupted: false
 * isInterrupted: true
 * 注意判断是否中断都是当前线程的，所以这里都是Thread的，都是false
 */
public class InterruptTest3 {
    public static void main(String[] args) throws InterruptedException {
        Thread threadOne=new Thread(new Runnable() {
            @Override
            public void run() {
               for(;;){

               }
            }
        });

        threadOne.start();
        //设置中断标志
        threadOne.interrupt();
        //获取中断标志
        System.out.println("isInterrupted: "+threadOne.isInterrupted());
        //获取中断标志并重置
        System.out.println("isInterrupted: "+threadOne.interrupted());
        //获取中断标志并重置
        System.out.println("isInterrupted: "+Thread.interrupted());
        //获取中断标志
        System.out.println("isInterrupted: "+threadOne.isInterrupted());
        threadOne.join();
        System.out.println("main is over");
    }
}
