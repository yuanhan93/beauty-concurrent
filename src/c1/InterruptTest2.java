package c1;

/**
 * 结果：
 * threadOne begin sleep for 2000 seconds
 * threadOne is interrupted while sleeping
 * main is over
 */
public class InterruptTest2 {
    public static void main(String[] args) throws InterruptedException {
        Thread threadOne=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("threadOne begin sleep for 2000 seconds");
                    Thread.sleep(2000000);
                    System.out.println("threadOne awaking");
                }catch (InterruptedException e){
                    System.out.println("threadOne is interrupted while sleeping");
                    return;
                }
                System.out.println("threadOne leaving normally");
            }
        });

        threadOne.start();
        Thread.sleep(1000);
        //打断子线程的休眠
        threadOne.interrupt();
        threadOne.join();
        System.out.println("main is over");
    }
}
