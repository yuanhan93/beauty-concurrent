package c1;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 1.使用Thread继承方式
 *   调用start方法并没有马上执行，而是处于就绪状态，线程已经获取了除CPU资源之外的其他资源，等待获取CPU资源才会真正处于运行状态。
 *   好处：获取当前线程直接this...坏处：已经继承了Thread；不能再继承其他类，任务与代码没分离
 * 2.Runnable接口
 *   可以继承其他类，可以执行多份代码
 * 3.FutureTask方式
 *   可以拿到任务的返回结果
 */
public class ThreadTest {
    public static void main(String[] args){
        MyThread myThread=new MyThread();
        myThread.start();

        RunnableTask task=new RunnableTask();
        new Thread(task).start();
        new Thread(task).start();

        //创建异步任务
        FutureTask<String> futureTask=new FutureTask<>(new CallerTask());
        //启动线程
        new Thread(futureTask).start();
        //等待任务执行完毕，返回结果
        String result= null;
        try {
            result = futureTask.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println(result);
    }
    public static class MyThread extends Thread{
        @Override
        public void run() {
            System.out.println("I am a child thread");
        }
    }

    public static class RunnableTask implements Runnable{

        @Override
        public void run() {
            System.out.println("I am a runnable.");
        }
    }

    public static class CallerTask implements Callable<String> {

        @Override
        public String call() throws Exception {
            return "hello";
        }
    }
}
