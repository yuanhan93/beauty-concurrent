package c1;

/**
 * ThreadLocal不支持继承性
 * main thread get:hello world
 * thread get:null
 * 父线程set,子线程get不到的，因为两个是不同的线程
 * 而InheritableThreadLocal可以
 * 取的是Thread中的inheritableThreadLocals
 */
public class ThreadLocalTest2 {
   // public static ThreadLocal<String> threadLocal=new ThreadLocal<>();
    public static ThreadLocal<String> threadLocal=new InheritableThreadLocal<>();
    public static void main(String[] args){
        threadLocal.set("hello world");
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("thread get:"+threadLocal.get());
            }
        });
        thread.start();
        System.out.println("main thread get:"+threadLocal.get());
    }

}
