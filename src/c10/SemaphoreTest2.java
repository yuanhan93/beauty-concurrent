package c10;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 如果构造参数Semaphore时传递的参数为N，并在M个线程调用了该信号量的release方法，那么在调用acquire
 * 使M个线程同步时传递的参数应该是M+N
 */
public class SemaphoreTest2 {
    private static Semaphore semaphore=new Semaphore(0);

    public static void main(String[] args) throws InterruptedException {
        ExecutorService es= Executors.newFixedThreadPool(2);
        es.submit(new Runnable() {
            @Override
            public void run() {
               System.out.println(Thread.currentThread()+" A task over");
               semaphore.release();
            }
        });
        es.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread()+" A task over");
                semaphore.release();
            }
        });

        semaphore.acquire(2);

        es.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread()+" B task over");
                semaphore.release();
            }
        });
        es.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread()+" B task over");
                semaphore.release();
            }
        });

        semaphore.acquire(2);

        System.out.println("all child thread over");
        es.shutdown();
    }
}
