package c10;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 全部线程都达到屏障点才能一块继续往下执行
 */
public class CyclicBarrierTest1 {
    private static CyclicBarrier cyclicBarrier=new CyclicBarrier(2, new Runnable() {
        @Override
        public void run() {
            System.out.println(Thread.currentThread()+" 出发去雪山 ");
        }
    });

    public static void main(String[] args) {
        ExecutorService es= Executors.newFixedThreadPool(2);
        es.submit(new Runnable() {
            @Override
            public void run() {

                try {
                    System.out.println(Thread.currentThread()+" task-1 ");
                    System.out.println(Thread.currentThread()+" enter in barrier ");
                    cyclicBarrier.await();
                    System.out.println(Thread.currentThread()+" out barrier ");
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        es.submit(new Runnable() {
            @Override
            public void run() {

                try {
                    System.out.println(Thread.currentThread()+" task-2 ");
                    System.out.println(Thread.currentThread()+" enter in barrier ");
                    cyclicBarrier.await();
                    System.out.println(Thread.currentThread()+" out barrier ");
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        es.shutdown();

    }

}
